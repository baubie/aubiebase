INSTALL_LIBS=true
DEV_MODE=false
CLEAN=false
HELP=false
DEPLOY_DIR=`pwd .`

while [ -n "$1" ]; do
    case "$1" in
        --dev) DEV_MODE=true ; shift ;;
        --nolibs) INSTALL_LIBS=false ; shift ;;
        --clean) CLEAN=true ; shift ;;
        --help) HELP=true ; shift ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

if [ "$HELP" = true ] ; then
    echo "Usage: deploy [--dev] [--nolibs] [--clean] [--help]"
    echo "--dev:  Symlink src directories to this folder for development."
    echo "--nolibs:  Skip installing libraries and applications if you've already done this."
    echo "--clean:  Clean out the sites-enabed and apps-enabled folders."
    echo "--help:  Disply this screen and quit."
    exit
fi

echo "AubieBase Deploy Script"
echo "-----------------------"
echo -n "Install Libraries: "
echo "$INSTALL_LIBS"
echo -n "Clean Nginx and uWSGI: "
echo "$CLEAN"
echo -n "Development Mode: "
echo "$DEV_MODE"
echo


if [ "$DEV_MODE" = false ] ; then
    echo -n "Please enter an application name (no spaces): "
    read APP_NAME
    if [ -z "${APP_NAME}" ]; then
        echo "Cannot be blank.  Cya."
        exit
    fi
fi

if [ "$DEV_MODE" = true ] ; then
    echo "Development mode requires the app to be called aubieapi."
    APP_NAME="aubieapi"
fi

echo -n "Please enter an application title (spaces allowed, default ${APP_NAME}): "
read APP_TITLE
if [ -z "${APP_TITLE}" ]; then
    APP_TITLE=${APP_NAME}
fi

echo -n "Please enter a domain name (default ${APP_NAME}.com): "
read SERVER_NAME
if [ -z "${SERVER_NAME}" ]; then
    SERVER_NAME="${APP_NAME}.com"
fi

echo -n "Please enter a listening port (default is 80): "
read SERVER_PORT
if [ -z "${SERVER_PORT}" ]; then
    SERVER_PORT="80"
fi


echo -n "Please enter a database name (default ${APP_NAME}): "
read DATABASE_NAME
if [ -z "${DATABASE_NAME}" ]; then
    DATABASE_NAME="$APP_NAME"
fi

echo -n "Please enter a root directory to install (default $HOME): "
read ROOT_DIR
if [ -z "${ROOT_DIR}" ]; then
    ROOT_DIR="$HOME"
fi


DATABASE_PASSWORD=`< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32};echo;`
SECRET_KEY=`< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32};echo;`
BASE_DIR=${ROOT_DIR}/${APP_NAME}

echo "Here we go!"
echo


if [ "$INSTALL_LIBS" = true ] ; then

    echo "Updating apt"
    echo "-------------------"
    sudo add-apt-repository -y ppa:nginx/stable
    sudo apt-get -y update


    echo "Installing packages"
    echo "-------------------"
    sudo apt-get install -y python3.4 python3-setuptools python3-pip
    sudo apt-get install -y postgresql-9.3
    sudo apt-get install -y postgresql-server-dev-9.3
    sudo apt-get install -y nginx
    sudo apt-get install -y redis-server
    sudo apt-get install -y ruby-sass

    sudo apt-get install libpcre3 libpcre3-dev
    sudo apt-get install -y uwsgi
    sudo pip3 install uwsgi
    sudo pip3 install virtualenv
    sudo mv /usr/bin/uwsgi /usr/bin/uwsgi-old
    sudo ln -s /usr/local/bin/uwsgi /usr/bin

    sudo cp ./conf/nginx/nginx.conf /etc/nginx
    sudo cp ./conf/postgres/pg_hba.conf /etc/postgresql/9.3/main/
fi


if [ "$CLEAN" = true ] ; then

    sudo rm /etc/nginx/sites-enabled/*
    sudo rm /etc/uwsgi/apps-enabled/*
fi

echo "Setting up System"
echo "-------------------"

sudo cp ./conf/nginx/aubiebase /etc/nginx/sites-available/${APP_NAME}
sudo ln -s /etc/nginx/sites-available/${APP_NAME} /etc/nginx/sites-enabled/${APP_NAME}

sudo cp ./conf/uwsgi/aubiebase.ini /etc/uwsgi/apps-available/${APP_NAME}.ini
sudo ln -s /etc/uwsgi/apps-available/${APP_NAME}.ini /etc/uwsgi/apps-enabled/${APP_NAME}.ini


sudo -u postgres psql -c "CREATE ROLE ${DATABASE_NAME} SUPERUSER LOGIN;"
sudo -u postgres psql -c "ALTER ROLE ${DATABASE_NAME} WITH PASSWORD '${DATABASE_PASSWORD}';"
sudo -u postgres psql -c "CREATE DATABASE ${DATABASE_NAME} OWNER ${DATABASE_NAME};"


echo "Setting up nginx..."

sudo sed -i "s/{{SERVER_NAME}}/${SERVER_NAME}/g" /etc/nginx/sites-available/${APP_NAME}
sudo sed -i "s/{{APP_NAME}}/${APP_NAME}/g" /etc/nginx/sites-available/${APP_NAME}
sudo sed -i "s|{{BASE_DIR}}|${BASE_DIR}|g" /etc/nginx/sites-available/${APP_NAME}
sudo sed -i "s/{{SERVER_PORT}}/${SERVER_PORT}/g" /etc/nginx/sites-available/${APP_NAME}


echo "Setting up uWSGI..."
sudo sed -i "s|{{BASE_DIR}}|${BASE_DIR}|g" /etc/uwsgi/apps-available/${APP_NAME}.ini
sudo sed -i "s/{{APP_NAME}}/${APP_NAME}/g" /etc/uwsgi/apps-available/${APP_NAME}.ini

echo "Setting up Application..."
mkdir -p ${BASE_DIR}
cp -r ../* ${BASE_DIR}

if [ "$DEV_MODE" = true ] ; then
    rm -r ${BASE_DIR}/api/src
    rm -r ${BASE_DIR}/app/src
    ln -s ${DEPLOY_DIR}/../api/src ${BASE_DIR}/api/src
    ln -s ${DEPLOY_DIR}/../app/src ${BASE_DIR}/app/src
fi


if [ "$DEV_MODE" = false ] ; then
    mv ${BASE_DIR}/api/src/aubieapi ${BASE_DIR}/api/src/${APP_NAME}
fi

cp ${BASE_DIR}/api/src/${APP_NAME}/settings.py.template ${BASE_DIR}/api/src/${APP_NAME}/settings.py
sed -i "s/{{SECRET_KEY}}/${SECRET_KEY}/g" ${BASE_DIR}/api/src/${APP_NAME}/settings.py
sed -i "s/{{DATABASE_NAME}}/${DATABASE_NAME}/g" ${BASE_DIR}/api/src/${APP_NAME}/settings.py
sed -i "s/{{DATABASE_PASSWORD}}/${DATABASE_PASSWORD}/g" ${BASE_DIR}/api/src/${APP_NAME}/settings.py

sed -i "s/aubieapi/${APP_NAME}/g" ${BASE_DIR}/api/src/wsgihandler.py
sed -i "s/aubieapi/${APP_NAME}/g" ${BASE_DIR}/api/src/createdb.py
sed -i "s/aubieapi/${APP_NAME}/g" ${BASE_DIR}/api/src/alembic/env.py

if [ "$DEV_MODE" = false ] ; then
    for fl in ${BASE_DIR}/api/src/${APP_NAME}/*.py; do
        sed -i "s/aubieapi/${APP_NAME}/g" $fl
    done
fi


sudo service postgresql restart
sudo service nginx restart
sudo service uwsgi restart

cd ${BASE_DIR}/api
. ./setup_venv
cd ${BASE_DIR}/api/src
python createdb.py
deactivate

cp ${BASE_DIR}/app/src/index.html.template ${BASE_DIR}/app/src/index.html
sed -i "s/{{APP_TITLE}}/${APP_TITLE}/g" ${BASE_DIR}/app/src/index.html

sudo service postgresql restart
sudo service nginx restart
sudo service uwsgi restart

echo
echo "------------------------"
echo "Script complete"
echo "Access localhost:${SERVER_PORT} to see your app (or ${SERVER_NAME}:${SERVER_PORT})"
echo
