angular.module('MyApp')
    .controller('UsersCtrl', function($scope, Users, $alert) {
        Users.getUsers().success(function(data) {


            $scope.users = data.data;
            $scope.roles = ['user', 'moderator', 'administrator', 'developer'];


            $scope.update_user = function(user, field) {

                var user_updated = {};
                user_updated[field] = user[field];

                Users.updateUser(user.user_id, user_updated).success(function(data) {
                  $alert({
                    title: 'Success!',
                    content: 'User '+field+' updated.',
                    animation: 'fadeZoomFadeDown',
                    type: 'material',
                    duration: 3
                  });
                }).error(function(data) {

                  $alert({
                    title: 'Error!',
                    content: data.errors[0],
                    animation: 'fadeZoomFadeDown',
                    type: 'material',
                    duration: 3
                  });
                });
            }

        });
    });
