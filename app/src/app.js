angular.module('MyApp', ['ngResource', 'ngMessages', 'ui.router', 'Satellizer', 'mgcrea.ngStrap', 'angularMoment'])

  .config(function($stateProvider, $urlRouterProvider, $authProvider) {

    $urlRouterProvider.otherwise("/");

    $stateProvider
      .state('dashboard', {
        url: '/',
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl'
      })
      .state('login', {
        url: '/login', 
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'views/signup.html',
        controller: 'SignupCtrl'
      })
      .state('logout', {
        url: '/logout', 
        template: null,
        controller: 'LogoutCtrl'
      })
      .state('profile', {
        url: '/profile', 
        templateUrl: 'views/profile.html',
        controller: 'ProfileCtrl'
      })
      .state('users', {
        url: '/users',
        templateUrl: 'views/users.html',
        controller: 'UsersCtrl'
      });


    /*
    $authProvider.facebook({
      clientId: '657854390977827'
    });

    $authProvider.google({
      clientId: '631036554609-v5hm2amv4pvico3asfi97f54sc51ji4o.apps.googleusercontent.com'
    });

    $authProvider.linkedin({
      clientId: '77cw786yignpzj'
    });

    $authProvider.github({
      clientId: '0ba2600b1dbdb756688b'
    });

    $authProvider.twitter({
      url: '/auth/twitter'
    });
    $authProvider.oauth2({
      name: 'foursquare',
      clientId: 'MTCEJ3NGW2PNNB31WOSBFDSAD4MTHYVAZ1UKIULXZ2CVFC2K',
      url: '/auth/foursquare',
      authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate',
      redirectUri: window.location.origin
    });
    */

  });
