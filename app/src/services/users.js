angular.module('MyApp')
  .factory('Users', function($http) {
    return {

      getUsers: function() {
        return $http.get('/api/users');
      },

      updateUser: function(user_id, user) {
        return $http.post('/api/users/'+user_id, user);
      }
    };
  });
