import aubieapi
aubieapi.db.drop_all()
aubieapi.db.create_all()

from aubieapi.user import User
user = User('developer@example.com')
user.change_password('developer')
user.role = 'developer'
aubieapi.db.session.add(user)
aubieapi.db.session.commit()
user.generate_string_id()
aubieapi.db.session.commit()

aubieapi.db.session.remove()

