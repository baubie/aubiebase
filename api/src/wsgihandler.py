import socket
import sys

from aubieapi import app
from aubieapi.settings import DEBUG

from werkzeug.debug import DebuggedApplication


application = app

if DEBUG:
    application.config['DEBUG'] = True
    application.config['EMAILDEBUG'] = True
    application = DebuggedApplication(application, evalex=True, show_hidden_frames=False)
    application.debug = True
else:
    application.config['EMAILDEBUG'] = False



if not application.debug:
    ADMINS = ['brandon@aubie.ca']
    import logging
    from logging.handlers import SMTPHandler
    from logging import Formatter
    from aubieapi.settings import MANDRILL_API, MANDRILL_USERNAME
    from aubieapi.logformat import QLogFormatter

    host = 'P'
    mail_handler = SMTPHandler(('smtp.mandrillapp.com', 587),
                               'brandon@aubie.ca', ADMINS,
                               '[%s] AubieAPI Error' % host, credentials=(MANDRILL_USERNAME, MANDRILL_API))

    mail_handler.setFormatter(QLogFormatter('''
    Message type:       %(levelname)s
    Location:           %(pathname)s:%(lineno)d
    Module:             %(module)s
    Function:           %(funcName)s
    Time:               %(asctime)s

    Message:

    %(message)s
    '''
    ))

    mail_handler.setLevel(logging.ERROR)
    application.logger.addHandler(mail_handler)
