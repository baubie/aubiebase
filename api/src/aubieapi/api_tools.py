import aubieapi

import re
import json
import datetime
from flask import Response, request
import decimal


def decimal_default(obj):
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    return str(obj)

def date_format(date):
    try:
        return date.strftime('%Y-%m-%dT%H:%M:%S.000Z')
    except AttributeError:
        return ''

def date_parse(date):
    try:
        return datetime.datetime.strptime(date, '%Y-%m-%dT%H:%M:%S.000Z')
    except ValueError:
        try:
            return datetime.datetime.fromtimestamp(int(date))
        except ValueError:
            return None


def time_parse(date):
    try:
        return datetime.datetime.strptime(date, '%H:%M:%S').time()
    except ValueError:
        return None


def verify_email(email):
    if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return False
    return True


def api_pagination():

    if 'pagenum' not in request.args or 'perpage' not in request.args:
        return 0, 100

    pagenum = int(request.args['pagenum'])
    perpage = int(request.args['perpage'])

    start = (pagenum-1)*perpage
    end = pagenum*perpage


    if start < 0 or start > end:
        start = 0

    if end < 0 or end < start:
        end = 0

    limit = perpage

    return start, limit


def api_package(status='success', status_code=200, data=None, errors=None, action="unknown", debug=None, numitems=None):
    """
    Generate a nice api package that follows the specifications of the BIRG API.

    Actions can be: created, modified, nochange, deleted, error, unknown

    """

    api_format, version = get_api_version()

    if api_format == 'json':  # pragma: no cover
        package = {}
        package['api_version'] = version
        package['status'] = status
        package['action'] = action


        if data is not None and numitems is None:  # Automatic pagination (warning, could be slow!)
            if isinstance(data, list):
                package['numitems'] = len(data)
                if 'perpage' in request.args and 'pagenum' in request.args:
                    try:
                        pagenum = int(request.args['pagenum'])
                        perpage = int(request.args['perpage'])
                        if pagenum > 0:
                            package['data'] = data[(pagenum-1)*perpage:pagenum*perpage]
                        else:
                            package['data'] = []
                    except ValueError:
                        package['data'] = data
                else:
                    package['data'] = data
                if len(package['data']) > 100:
                    package['data'] = package['data'][0:100]
            else:
                package['data'] = data

        elif numitems is not None:  # The list was pre-processed for pagination (for speed!)
            package['data'] = data
            package['numitems'] = numitems




        if errors is not None:
            package['errors'] = errors
            package['action'] = 'error'

        if debug is not None and (aubieapi.app.config['TESTING'] or aubieapi.app.config['DEBUG']):
            package['debug'] = debug

        js = json.dumps(package, default=decimal_default)
        resp = Response(js, status=status_code, mimetype='application/json')
        resp.headers.add('Cache-Control', 'no-cache')

        from .authentication import extract_authorization_token, session_expiration
        token = extract_authorization_token()
        if token is not None:
            expiration = session_expiration(token)
            resp.headers.add('X-Token-Expire', expiration)
        return resp
    return None


## Error Generating Function
def api_error(errors, status=400):

    if type(errors) != type([]):
        errors = [errors]

    return api_package(status='failure',
                       status_code=status,
                       errors=errors)

## Wrapper Error Generating Function for missing fields
def api_validation_error(errors):
    msg = ''
    for err in errors:
        msg += str(errors[err]) + ' ('+str(err)+'), '
    return api_error(msg, status=422)

## Wrapper Error Generating Function for missing fields
def api_error_missing(missing):
    return api_error("Missing field: %s" % missing, status=422)

## Wrapper Error Generating Function for schedule conflicts
def api_conflict(conflict):
    return api_error("Conflict with: %s" % conflict, status=409)

## Wrapper Error Generating Function for missing fields
def api_not_found(item):
    return api_error("%s not found" % item, status=404)

## Wrapper Error Generating Function for authentication issues
def api_authentication_error(msg=""):
    return api_error("Authentication required (Attempted: %s)" % msg,
                     status=401
                       )

def api_forbidden(msg=""):
    return api_error("Access Denied: %s " % msg,
                     status=403
                       )


def get_api_version():
    """
    Determine what resource type was requested in an API request.

    Returns a tuple with the first position a string represeting the type (json, xml, etc.) and the second the version.

    Looks in the Accept header for:
        application/vnd.mcmaster.birg-v0+json        <-- Internal version in JSON
        application/vnd.mcmaster.birg-v1+json        <-- Version 1 in JSON
        application/vnd.mcmaster.birg+json           <-- Latest version in JSON

    """

    #valid_formats = ['json']
    #valid_versions = [0, 1]

    #default_format = 'json'
    #default_version = 1

    api_format = 'json'
    version = 1
    return (api_format, version)
