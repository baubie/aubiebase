import aubieapi
from . import api_tools
from .authentication import ipthrottle, authenticate, authenticated_user, generate_token
from .settings import secret_key
from flask import request, jsonify


@aubieapi.app.errorhandler(404)
def api_404(e):  #pylint: disable=W0613
    return api_tools.api_error("API not found", 404)


@aubieapi.app.errorhandler(500)
def api_500(e):  #pylint: disable=W0613
    return api_tools.api_error("Server error. Administrators have been notified.", 500)



@aubieapi.api.route('/errormebro', endpoint="api_error_me_bro", methods=['GET'])
@ipthrottle()
def api_error_me_bro():
    raise Exception("You requested an error, sir?")


@aubieapi.api.route('/', endpoint="api_test_auth", methods=['GET'])
@ipthrottle()
@authenticate()
def api_test_auth():
    return api_tools.api_package()





@aubieapi.auth.route('/login', endpoint="auth_login", methods=['POST'])
@ipthrottle()
def api_session():
    """
    Request and generate a new user session
    """
    form = request.get_json()
    data = generate_token(username=form['email'], password=form['password'])
    if data is False:
        return api_tools.api_authentication_error('Bad credentials')
    return jsonify(data)
