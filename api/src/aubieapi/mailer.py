import aubieapi
from .settings import MANDRILL_API, secret_key, base_url, DEV_EMAIL_REDIRECT, FROM_EMAIL, FROM_NAME

import mandrill
from sqlalchemy import Column, Index, Integer, Boolean, DateTime, String, ForeignKey
from sqlalchemy.orm import relationship
import hashlib
from datetime import datetime
from itsdangerous import URLSafeTimedSerializer

def ping_mandrill():
    mapi = mandrill.Mandrill(MANDRILL_API)
    try:
        mapi.users.ping()
    except mandrill.Error:
        return False
    return True



def send(recipient, subject, html, plaintext, tags, files=None, from_override=None, replyto=None, subject_decoration='', subaccount=None, return_id=False):

    if aubieapi.app.config['TESTING'] is True:
        return True

    mapi = mandrill.Mandrill(MANDRILL_API)

    message = {
        'to': [{'email': recipient['email'],
                'name': recipient['name'],
                'type': 'to'}],
        'subject': subject_decoration + subject,
        'html': html,
        'from_email': FROM_EMAIL,
        'from_name':  FROM_NAME,
        'tags': tags
            }


    # Ensure they aren't unsubscribed
    if is_unsubscribed(message['to'][0]['email']):
        return False, "Unsubscribed"


    if plaintext is not None:
        message['text'] = plaintext
    else:
        message['auto_text'] = True

    if files is not None:
        message = add_files(message, files)

    if subaccount is not None:
        message['subaccount'] = subaccount

    if from_override is not None:
        message['from_email'] = from_override['email']
        message['from_name'] = from_override['name']

    if replyto is not None:
        if 'headers' not in message:
            message['headers'] = {}
        message['headers']['Reply-To'] = "%s <%s>" % (replyto['name'], replyto['email'])
        message['from_name'] = '%s via QReserve' % replyto['name']
        message['headers']['Sender'] = FROM_EMAIL



    if aubieapi.app.config['EMAILDEBUG'] is True:
        message['to'][0]['email'] = DEV_EMAIL_REDIRECT
        html = '<div style="border: 2px solid #A00; background-color: #FFCFCF; padding: 5px;"><strong>Developer Redirect</strong><br>Original To: %s &lt;%s&gt;</div>' %  (recipient['name'], recipient['email']) + html

    try:
        result = mapi.messages.send(message=message, async=False)
        if result[0]['status'] != 'sent' and result[0]['status'] != 'queued':
            return result[0]['status'] + ' Reason: ' + result[0]['reject_reason']
        message_id = None
        if '_id' in result[0]:
            message_id = result[0]['_id']

    except mandrill.Error as e:
        return e

    if return_id:
        return True, message_id
    else:
        return True


