import aubieapi
import datetime
from . import exceptions as ex
from flask import request
from sqlalchemy import Column, Integer, Text, String, DateTime, String, ForeignKey
from sqlalchemy.orm import relationship

def clean_post(data):
    """remove references to passwords"""
    if data:
        if 'password' in data:
            data['password'] = "****"
    return data

class Log(aubieapi.db.Model):

    __tablename__ = 'log'
    log_id = Column(Integer, primary_key=True)
    created = Column(DateTime, default=datetime.datetime.utcnow)
    user = Column(Integer, ForeignKey('user.user_id'))
    request_url = Column(String(255))
    request_body = Column(Text)
    msg = Column(Text)
    item_id = Column(Integer)
    item_id2 = Column(Integer)
    ip = Column(String(15))
    code = Column(String(64))
    kind = Column(String(2))

    user_obj = relationship('User')

    # Kinds: E) Error
    #        W) Warning
    #        M) Message
    #        D) Debug

    def as_dict(self):
        from api_tools import date_format
        r = {}
        r["created"] = date_format(self.created)
        if self.user_obj is not None:
            r["user"] = self.user_obj.as_dict()

        r["request_url"] = self.request_url
        r["msg"] = self.msg
        r["item_id"] = self.item_id
        r["item_id2"] = self.item_id2
        r["ip"] = self.ip
        r["code"] = self.code
        r["kind"] = self.kind

        return r


    @classmethod
    def find(cls, code=None, kind=None, user_id=None, item_id=None, ip=None, start_date=None, end_date=None, count_only=False):

        q = cls.query
        if code is not None:
            q = q.filter(Log.code == code)

        if kind is not None:
            q = q.filter(Log.kind == kind)

        if user_id is not None:
            q = q.filter(Log.user == user_id)

        if ip is not None:
            q = q.filter(Log.ip == str(ip)[0:15])

        if start_date is None:
            start_date = datetime.datetime.utcnow() - datetime.timedelta(days=1)

        if end_date is None:
            end_date = datetime.datetime.utcnow()

        q = q.filter(Log.created >= start_date)
        q = q.filter(Log.created <= end_date)

        if count_only:
            return q.count()

        else:
            return q.all()


    @staticmethod
    def logevent(msg, code, kind='M', item_id=None, item_id2=None, add_json=False):
        from .authentication import authenticated_user
        newlog = Log()
        newlog.kind = kind
        newlog.msg = msg
        newlog.code = code
        newlog.item_id = item_id
        newlog.item_id2 = item_id2
        newlog.ip = str(request.remote_addr)[0:15]
        try:
            newlog.user = authenticated_user().user_id
        except ex.AuthenticationError as e:
            newlog.user = None
        newlog.request_url = request.url

        if add_json:
            try:
                newlog.request_body = str(clean_post(request.get_json(force=True, silent=True)))
            except Exception as e:  #pylint: disable=W0703
                newlog.request_body = str(e)

        aubieapi.db.session.add(newlog)
        aubieapi.db.session.commit()
