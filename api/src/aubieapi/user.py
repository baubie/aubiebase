import aubieapi

from .settings import secret_key, base_url, api_base_url
from .log import Log
from .authentication import check_ipthrottle
from . import exceptions as ex
from . import mailer

import hashlib
import time
import urllib
import random
from flask import request
from sqlalchemy.orm import aliased, relationship
from sqlalchemy import Column, Index, Integer, Boolean, DateTime, String, ForeignKey, or_, func
from werkzeug import generate_password_hash, check_password_hash
from datetime import datetime, timedelta
from itsdangerous import URLSafeSerializer, URLSafeTimedSerializer, BadSignature, SignatureExpired


class User(aubieapi.db.Model):

    __tablename__ = 'user'
    user_id = Column(Integer, primary_key=True)
    user_string_id = Column(String(40))

    email = Column(String(60), nullable=False)
    pwdhash = Column(String())
    pwdexpiry = Column(DateTime, nullable=True)

    surname = Column(String(255))
    givennames = Column(String(255))
    created = Column(DateTime, default=datetime.utcnow)
    last_access = Column(DateTime)
    website = Column(String(255))

    display_name_override = Column(String(255), default='')
    role = Column(String(32), default="user")

    active = Column(Boolean, default=True)

    def __init__(self, email):
        self.email = email.lower()

    @classmethod
    def authquery(cls):
        from .authentication import authenticated_user

        try:
            user = authenticated_user()
        except ex.AuthenticationError:
            return cls.query.filter(User.user_id == -1)  # Impossible user will return empty list

        if user.role == 'developer':  # pragma: no cover
            return cls.query

        #  ALL USERS CAN VIEW ALL USERS BY DEFAULT
        #  ADD LOGIC HERE TO RESTRICT THE QUERY
        q = cls.query

        return q


    @classmethod
    def from_email(cls, email):
        user = User.query.filter(func.lower(User.email) == email.lower()).first()
        if user is not None:
            return user
        else:
            user_email = UserEmail.query.filter(func.lower(UserEmail.email) == email.lower()).filter(UserEmail.verified == True).first()
            if user_email is not None:
                return user_email.user
        return None

    def generate_string_id(self):
        import base64
        self.user_string_id = base64.urlsafe_b64encode(hashlib.sha1(b"USER"+bytes(self.user_id)+secret_key.encode('utf-8')).digest()).decode("ascii")


    def change_password(self, password):
        self.pwdhash = generate_password_hash(password)

    def display_name(self):
        r = ''
        if self.display_name_override is not None and self.display_name_override.strip() != '':
            return self.display_name_override.strip()
        if self.givennames is not None and self.surname is not None:
            r = self.givennames + ' ' + self.surname
        if r.strip() == '':
            r = self.email
        return r.strip()

    def firstname(self):
        if self.givennames is not None and self.givennames != '':
            return self.givennames
        if len(self.display_name().split(' ')) > 1:
            return self.display_name().split(' ')[0]
        return self.display_name()

    def alt_emails(self, include_unverified=False):

        q = UserEmail.query.filter(UserEmail.user_id == self.user_id)
        if not include_unverified:
            q = q.filter(UserEmail.verified == False)
        return q.all()

    def as_dict(self, include_email = False):

        from .authentication import authenticated_user
        from .api_tools import date_format
        result = {}

        result['user_id'] = self.user_string_id
        result['givennames'] = self.givennames
        result['lastname'] = self.surname
        result['display_name'] = self.display_name()
        result['created'] = date_format(self.created)
        result['role'] = self.role
        result['website'] = self.website
        result['authenticatable'] = self.pwdhash is not None and self.active
        result['active'] = self.active
        result['last_access'] = ''
        if self.last_access is not None:
            result['last_access'] = date_format(self.last_access)

        auth_user = None
        try:
            auth_user = authenticated_user()
        except ex.AuthenticationError:
            pass

        try:
            if auth_user is not None and (self == auth_user or auth_user.role == 'developer'):
                result['email'] = self.email
                result['alt_emails'] = [e.as_dict() for e in self.alt_emails(include_unverified=True)]
        except ex.AuthenticationError:
            pass

        if include_email:
            result['email'] = self.email

        return result

    @classmethod
    def from_string_id(cls, userid):
        return cls.query.filter(cls.user_string_id == str(userid)).first()

    @classmethod
    def from_credentials(cls, email, password):

        user = cls.from_email(email)
        if user is not None:
            if user.active and user.check_password(password):
                return user

        return None

    def mark_access(self):
        # Only update this once per minute
        if self.last_access is None or self.last_access < datetime.utcnow() - timedelta(seconds=60):
            self.last_access = datetime.utcnow()
            aubieapi.db.session.commit()

    def check_password(self, password):
        if self.pwdhash is not None and check_password_hash(self.pwdhash, password):

            if self.pwdexpiry is not None and self.pwdexpiry < datetime.now():
                return False  # Expired password

            return True

        return False



    def initiate_password_reset(self, password, newuser=False, email=None):

        if email is None:
            email = self.email

        if type(password) != type('') and type(password) != type(u''):
            return False, ''

        pwdreset = UserPasswordReset(self.user_id)
        pwdreset.pwdhash = generate_password_hash(password)

        # Setup the temporary password to expire in 24 hours
        self.pwdhash = pwdreset.pwdhash
        self.pwdexpiry = datetime.now()+timedelta(days=1)

        aubieapi.db.session.add(pwdreset)
        aubieapi.db.session.commit()

        recipient = {'name': self.display_name(), 'email': email}
        subject = None

        if newuser is True:
            template_name = 'new-user-password-reset'
            subject = "Password Setup"
            tags = ["new user"]
            url = base_url+'/#/newuser/'+pwdreset.get_token()
        else:
            template_name = 'password-reset'
            subject = "Password Reset Requested"
            tags = ["password reset"]
            url = base_url+'/#/resetpassword/'+pwdreset.get_token()

        merge_vars = [{'name': 'firstname', 'content': self.givennames},
                      {'name': 'changepasswordurl', 'content': url}
                     ]


        r = mailer.send_template(recipient,
                            subject,
                            template_name,
                            merge_vars,
                            tags)

        if r is not True:
            return False, pwdreset.get_token()

        return True, pwdreset.get_token()

    def sendmessage(self, from_user, subject, message, copyself=True, reservable=None, site=None):
        """
        from_user can be type User or type list.  When a list, the format is ["Display Name", "email@address.com"].
        """

        if check_ipthrottle(LIMIT=5, TIMELIMIT=60, NAME='sendmessage') is False:
            return False, "Too many sends per minute"

        from_display_name = ''
        from_email = ''
        if type(from_user) == list:
            from_display_name = from_user[0]
            from_email = from_user[1]
        elif type(from_user) == User:
            from_display_name = from_user.display_name()
            from_email = from_user.email
        else:
            return False, "Invalid from user format"

        recipient = {'name': self.display_name(), 'email': self.email}
        template_name = 'send-message'
        message = message.replace('\n', '<br>')

        merge_vars = [{'name': 'firstname', 'content': self.givennames},
                      {'name': 'fromname', 'content': from_display_name},
                      {'name': 'message', 'content': message}
                     ]

        if reservable is not None:
            merge_vars.append({'name': 'sitename', 'content': reservable.site.name})
            merge_vars.append({'name': 'reservable', 'content': reservable.name})
        elif site is not None:
            merge_vars.append({'name': 'sitename', 'content': site.name})

        tags = ["send message"]

        if copyself:
            copy_recipient = {'name': from_display_name, 'email': from_email}
            copy_merge_vars = list(merge_vars)
            copy_merge_vars.append({'name': 'copy', 'content': 'copy'})
            r = mailer.send_template(copy_recipient,
                            subject,
                            template_name,
                            copy_merge_vars,
                            tags)
            if r is not True:
                return False, "Error delivering self copied email. Will not deliver original message either."

        r = mailer.send_template(recipient,
                            subject,
                            template_name,
                            merge_vars,
                            tags,
                            replyto={'email': from_email, 'name': from_display_name})



        if r is not True:
            return False, "Error sending email"

        Log.logevent('', 'SENDMESSAGE', 'M', self.user_id)
        return True, None


class UserEmail(aubieapi.db.Model):

    __tablename__ = 'user_email'
    user_email_id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.user_id'))
    email = Column(String(60), nullable=False)
    verified = Column(Boolean, default=False)
    promote_to_primary = Column(Boolean, default=False)
    user = relationship(User)

    def as_dict(self):
        r = {}
        r['email'] = self.email
        r['verified'] = self.verified
        return r

    def get_token(self):
        s = URLSafeSerializer(secret_key, salt='user_email')
        token = s.dumps(self.user_email_id)
        return token

    @classmethod
    def from_token(cls, token):
        s = URLSafeSerializer(secret_key, salt='user_email')
        try:
            ue_id = s.loads(token)
            q = cls.query
            q = q.filter(cls.user_email_id == ue_id)
            ue = q.first()
            return ue
        except ValueError:
            return None
        except BadSignature:
            return None

    def send_verification(self):
        user = User.query.filter(User.user_id == self.user_id).first()
        if user is None:
            return False

        recipient = {'name': user.display_name(), 'email': self.email}
        subject = None

        template_name = 'verify-email'
        subject = "Email Verification"

        merge_vars = [
                      {'name': 'verifyurl', 'content': base_url+'/#/verifyemail/'+self.get_token()}
                     ]

        tags = ["email verification"]

        r = mailer.send_template(recipient,
                            subject,
                            template_name,
                            merge_vars,
                            tags)

        if r is not True:
            return False
        return True


    @classmethod
    def create(cls, user_id, email, promote_to_primary):
        """
        Create a new user email.
        This function does NOT do any error checking besides the default foreign key checks.
        Syntax and other error checking is meant to be done at the caller.

        Returns False when the email failed to send otherwise returns True.
        """

        # Are we just resending the verification?

        ue = cls.query.filter(cls.email == email).filter(cls.user_id == user_id).filter(cls.verified == False).first()

        if ue is None:
            ue = cls()
            ue.user_id = user_id
            ue.email = email
            ue.promote_to_primary = promote_to_primary
            ue.verified = False
            aubieapi.db.session.add(ue)
            aubieapi.db.session.flush()

        if ue.send_verification() is False:
            return False

        #  Only add them to the database after a successful email sent
        aubieapi.db.session.commit()
        return True


class UserPasswordReset(aubieapi.db.Model):

    __tablename__ = 'user_password_reset'
    user_password_id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.user_id'))
    pwdhash = Column(String())
    created = Column(DateTime, default=datetime.utcnow)

    def __init__(self, user_id):
        self.user_id = user_id

    def get_token(self):
        s = URLSafeTimedSerializer(secret_key, salt='user_email')
        token = s.dumps(self.user_password_id)
        return token

    @classmethod
    def from_token(cls, token):
        s = URLSafeTimedSerializer(secret_key, salt='user_email')
        try:
            ue_id = s.loads(token, max_age = 60*60*24*7)  # Password reset tokens are good for a week
            q = cls.query
            q = q.filter(cls.user_password_id == ue_id)
            ue = q.first()
            return ue
        except SignatureExpired:
            return None
        except ValueError:
            return None
        except BadSignature:
            return None
