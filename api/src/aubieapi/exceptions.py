
class AuthenticationError(Exception):
    pass

class DataError(Exception):
    pass
