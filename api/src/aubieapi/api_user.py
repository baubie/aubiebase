import aubieapi
from .log import Log
from .settings import secret_key
from .user import User, UserPasswordReset, UserEmail
from .authentication import authenticate, authenticate_local, ipthrottle, authenticated_user, auth_roles
from .api_tools import api_package, api_error, api_error_missing, api_not_found, verify_email, api_forbidden, api_validation_error

import datetime
from flask import request, Response
from cerberus import Validator
from sqlalchemy import or_, and_
from itsdangerous import URLSafeTimedSerializer


@aubieapi.api.route('/users', endpoint="api_users", methods=['GET'])
@ipthrottle()
@authenticate(auth_roles['moderator'])
def api_users():
    users = User.query.all()
    return api_package(data=[u.as_dict() for u in users])




@aubieapi.api.route('/me', endpoint="api_user_self", methods=['GET'])
@ipthrottle()
@authenticate()
def api_user_self():
    user = authenticated_user()
    return api_package(data=user.as_dict())





@aubieapi.api.route('/users/<string:user_id>', endpoint="api_user", methods=['GET'])
@ipthrottle()
@authenticate()
def api_user(user_id):
    """
    Return a user.
    """

    q = User.authquery()
    q = q.filter(User.user_string_id == user_id)
    user = q.first()
    if user is None:
        return api_not_found("User")

    return api_package(data=user.as_dict())




@aubieapi.api.route('/users/<string:user_id>', endpoint="api_user_edit", methods=['POST'])
@ipthrottle()
@authenticate()
def api_user_edit(user_id):
    """
    Edit a user.
    """

    q = User.authquery()
    q = q.filter(User.user_string_id == user_id)
    user = q.first()

    auth_user = authenticated_user()

    form = request.get_json()

    # Validate JSON
    if form is None:
        return api_error("Invalid request")

    schema = {
              'givennames': {'type': 'string'},
              'lastname': {'type': 'string'},
              'display_name': {'type': 'string'},
              'website': {'type': 'string'},
              'role': {'type': 'string'},
              'active': {'type': 'boolean'},
             }

    v = Validator(schema)
    if v.validate(form) is False:
        return api_validation_error(v.errors)

    if user is None:
        return api_not_found("User")

    if user.user_id != auth_user.user_id and auth_user.role not in auth_roles['administrator']:
        return api_error("You do not have permission to edit this user.")

    if "givennames" in form:
        user.givennames = form["givennames"]
    if "display_name" in form:
        user.display_name_override = form["display_name"]
    if "lastname" in form:
        user.surname = form["lastname"]
    if 'website' in form:
        user.website = form["website"]

    if 'role' in form:
        if user.user_id == auth_user.user_id and user.role != form['role']:
            return api_error('You cannot modify the role of yourself.')
        user.role = form["role"]

    if 'active' in form:
        if user.user_id == auth_user.user_id:
            return api_error('You cannot modify the active state of yourself.')
        user.active = form["active"]

    aubieapi.db.session.commit()

    return api_package(data=user.as_dict())


@aubieapi.api.route('/addemail', endpoint="api_user_addemail", methods=['POST'])
@ipthrottle()
@authenticate()
def api_user_addemail():
    """
    Add an email address to a user
    """

    auth_user = authenticated_user()
    form = request.get_json()

    # Validate JSON
    if form is None:
        return api_error("Invalid request")

    schema = {
              'email': {'type': 'string', 'empty': False},
              'makeprimary': {'type': 'boolean'}
             }

    v = Validator(schema)
    if v.validate(form) is False:
        return api_validation_error(v.errors)

    if not verify_email(form["email"]):
        return api_error("Invalid email address", 422)

    # Verify nobody else has this email address
    user = User.from_email(form["email"])
    if user is not None:
        return api_error("Email address already in use.")


    r = UserEmail.create(auth_user.user_id, form["email"], form["makeprimary"])

    if r:
        return api_package(data=auth_user.as_dict(), action="created")
    else:
        return api_error("Unable to deliver verification email")


@aubieapi.api.route('/removeemail', endpoint="api_user_removeemail", methods=['POST'])
@ipthrottle()
@authenticate()
def api_user_removeemail():
    """
    Remove an email address to a user
    """

    auth_user = authenticated_user()

    form = request.get_json()

    # Validate JSON
    if form is None:
        return api_error("Invalid request")

    schema = {
              'email': {'type': 'string', 'empty': False}
             }

    v = Validator(schema)
    if v.validate(form) is False:
        return api_validation_error(v.errors)

    ue = UserEmail.query.filter(UserEmail.user_id == auth_user.user_id).filter(UserEmail.email == form["email"]).first()
    if ue is None:
        return api_error("This email does not appear to be registered on your account.")

    aubieapi.db.session.delete(ue)
    aubieapi.db.session.commit()

    return api_package(action="deleted")


@aubieapi.api.route('/verifyemail', endpoint="api_user_verifyemail", methods=['POST'])
@ipthrottle()
def api_user_verifyemail():
    """
    Verify an email address to a user
    """

    form = request.get_json()

    # Validate JSON
    if form is None:
        return api_error("Invalid request")

    schema = {
              'token': {'type': 'string', 'empty': False}
             }

    v = Validator(schema)
    if v.validate(form) is False:
        return api_validation_error(v.errors)

    ue = UserEmail.from_token(form['token'])
    if ue is None:
        return api_error("This email verification token is either already used or invalid.")

    ue.token = ''
    ue.verified = True

    if ue.promote_to_primary:
        # Swap the primary and this new one.  No need to create a whole new one.
        old_email = ue.user.email
        ue.user.email = ue.email
        ue.email = old_email

    aubieapi.db.session.commit()

    return api_package(action="modified")


@aubieapi.auth.route('/signup', methods=['POST'])
def signup():
    u = User(email=request.json['email'])
    u.change_password(request.json['password'])
    u.role = 'user'
    aubieapi.db.session.add(u)
    aubieapi.db.session.commit()
    u.generate_string_id()
    aubieapi.db.session.commit()
    return api_package(data=u.as_dict())


@aubieapi.api.route('/initiatepasswordreset', endpoint="api_initiate_password_reset", methods=['POST'])
@ipthrottle()
def api_initiate_password_reset():

    form = request.get_json()

    # Validate JSON
    if form is None:
        return api_error("Invalid request")
    required = ["email", "password"]
    for req in required:
        if req not in form:
            return api_error_missing(req)

    if type(form["password"]) != type([1,]):
        return api_error("Password field must be a list of two passwords", 422)
    if len(form["password"]) != 2:
        return api_error("Password field must be a list of two passwords", 422)
    if form["password"][0] != form["password"][1]:
        return api_error("Passwords must match", 422)

    user = User.from_email(form["email"])
    if user is None:
        return api_not_found("User")

    security_token = None
    if 'token' in form:
        security_token = form['token']

    if security_token is not None:
        # Verify token
        import hashlib
        test_token = hashlib.sha1(form['email'] + secret_key + 'newuserverify').hexdigest()

        if security_token == test_token:
            user.change_password(form['password'][0])
            user.pwdexpiry = datetime.datetime.now() + datetime.timedelta(days=365*100)
            Log.logevent('', 'SECUREINITPWDRESET', 'M', user.user_id)
            return api_package(data={'event': 'complete'})


    success, token = user.initiate_password_reset(form["password"][0], email=form["email"])

    Log.logevent('', 'INITPWDRESET', 'M', user.user_id)

    if success is not True:
        return api_error("Error delivering email.")

    if aubieapi.app.config['TESTING'] is True or aubieapi.app.config['DEBUG'] is True:  # pragma: no cover
        return api_package(data=token)

    return api_package(data={'event': 'init'})


@aubieapi.api.route('/resetpassword', endpoint="api_reset_password", methods=['POST'])
@ipthrottle()
def api_reset_password():

    form = request.get_json()

    # Validate JSON
    if form is None:
        return api_error("Invalid request")
    required = ["token"]
    for req in required:
        if req not in form:
            return api_error_missing(req)

    resetrequest = UserPasswordReset.from_token(form["token"])

    if resetrequest is None:
        return api_error("Invalid or expired (>72 hours) token", 400)

    user = User.query.filter(User.user_id == resetrequest.user_id).first()
    user.pwdhash = resetrequest.pwdhash
    user.pwdexpiry = datetime.datetime.now() + datetime.timedelta(days=365*100)
    aubieapi.db.session.delete(resetrequest)
    aubieapi.db.session.commit()

    Log.logevent('', 'PWDRESET', 'M', user.user_id)

    return api_package()


@aubieapi.api.route('/users/<string:user_id>/sendmessage', endpoint="api_user_sendmessage", methods=['POST'])
@ipthrottle()
@authenticate()
def api_user_sendmessage(user_id):
    """
    Send a message to a user.
    """

    from qsite import Site
    from reservable import Reservable

    auth_user = authenticated_user()

    form = request.get_json()

    # Validate JSON
    if form is None:
        return api_error("Invalid request")

    schema = {
              'site_id': {'type': 'string', 'empty': False},
              'reservable_id': {'type': 'string', 'empty': False},
              'copyself': {'type': 'boolean', 'required': True},
              'subject': {'type': 'string', 'required': True},
              'message': {'type': 'string', 'required': True}
             }

    v = Validator(schema)
    if v.validate(form) is False:
        return api_validation_error(v.errors)


    users = []
    if user_id not in ['developers', 'support']:
        q = User.authquery()
        q = q.filter(User.user_string_id == user_id)
        user = q.first()
        if user is None:
            return api_not_found("User")

        reservable = None
        site = None
        users = [user]

    else:
        q = User.query
        q = q.filter(User.role == user_id)
        users = q.all()

    for user in users:
        if 'reservable_id' in form:
            reservable = Reservable.authquery().filter(Reservable.reservable_string_id == form['reservable_id']).first()

        if 'site_id' in form:
            site = Site.authquery().filter(Site.site_string_id == form['site_id']).first()


        r, error = user.sendmessage(from_user=auth_user,
                                    subject=form['subject'],
                                    message=form['message'],
                                    copyself=form['copyself'],
                                    reservable=reservable,
                                    site=site)

    if r:
        return api_package(action="unchanged")
    else:
        return api_error(error)


