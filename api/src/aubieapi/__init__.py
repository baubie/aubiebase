from . import settings
import redis
from flask import Flask, Blueprint
from flask.ext.sqlalchemy import SQLAlchemy

# strptime doesn't like multi-threaded apps sometimes, so import it here to get it done with
import datetime
throwaway = datetime.datetime.strptime('20110101','%Y%m%d')

## Create the Flask app
app = Flask(__name__)
api = Blueprint('api', __name__)
auth = Blueprint('auth', __name__)


## Setup database
## We use Flask-SqlAlchemy here which automatically removes DB sessions at the end of each request
app.config['SQLALCHEMY_DATABASE_URI'] = settings.connectstring
db = SQLAlchemy(app)

## Setup redis
r = redis.StrictRedis(host=settings.REDIS_SERVER, port=settings.REDIS_PORT, db=settings.REDIS_DB)

import aubieapi.api_authentication
import aubieapi.api_user
import aubieapi.log


app.register_blueprint(api, url_prefix='/api')
app.register_blueprint(auth, url_prefix='/auth')
