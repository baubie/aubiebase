from .log import Log, clean_post
from flask import request
import logging

class QLogFormatter(logging.Formatter):
    def __init__(self, fmt):
        logging.Formatter.__init__(self, fmt)

    def format(self, record):
        msg = logging.Formatter.format(self, record)
        log_output = "\n".join([x for x in msg.split("\n")])
        post_data = "\n\nPOST DATA (JSON): \n\n %s" % str(clean_post(request.json))
        self.qreserve_log("%s %s" % (log_output, post_data))
        return "%s %s" % (log_output, post_data)

    def qreserve_log(self, msg):
        try:
            Log.logevent(msg, 'EXCEPTION', 'E', add_json=True)
        except Exception as e:  # pylint: disable=W0703
            print("Error logging exception")
            print(e)

