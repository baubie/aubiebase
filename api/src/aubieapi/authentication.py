import aubieapi
from . import exceptions as ex
from .settings import secret_key
from .log import Log
from .api_tools import date_format, api_error, api_authentication_error

import time
from flask import request
import datetime
import jwt
import base64
import hashlib
from itsdangerous import URLSafeTimedSerializer, BadSignature, SignatureExpired


"""
The authenticate() decorator takes an optional list parameter listing the roles allowed to access the API.
The default is that all logged in users can access the API.
The auth_roles dict() below is for convenience to allow a particular role and all higher to access an API.

For example:
@authenticate(auth_roles['moderator']) -> @authenticate(['moderator', 'administrator', 'developer'])
"""
auth_roles = {}
auth_roles['user'] = ['user', 'moderator', 'administrator', 'developer']
auth_roles['moderator'] = ['moderator', 'administrator', 'developer']
auth_roles['administrator'] = ['administrator', 'developer']
auth_roles['developer'] = ['developer']


def session_expiration(token):
    try:
        _, claims = jwt.decode(token, secret_key)
        ts = claims["exp"]
        return date_format(datetime.datetime.utcfromtimestamp(ts))
    except ValueError:
        return date_format(datetime.datetime.utcfromtimestamp(0))
    except jwt._JWTError:
        return date_format(datetime.datetime.utcfromtimestamp(0))


def extract_authorization_token():
    if 'Authorization' in request.headers:
        token = str(request.headers['Authorization'].split(' ')[1].strip())
        return token
    return None


def generate_token(username=None, password=None):

    from .user import User
    user = User.from_credentials(username, password)

    if user is None:
        return False

    EXPIRY_TIME = datetime.datetime.utcnow()+datetime.timedelta(minutes=60*24)
    payload = {'user': {'user_id': user.user_id,
                        'email': user.email,
                        'role': user.role,
                        'user': user.role in auth_roles['user'],
                        'moderator': user.role in auth_roles['moderator'],
                        'administrator': user.role in auth_roles['administrator'],
                        'developer': user.role in auth_roles['developer']
                       },
                'ip': request.remote_addr,
                'exp': EXPIRY_TIME
                }
    token = jwt.encode(payload, secret_key, 'HS512' ).decode('ascii')
    data = {"token": token}
    return data

def authenticated_user():

    from .user import User

    # Check for basic auth
    auth = request.authorization
    if auth is not None:
        user = User.from_credentials(auth['username'], auth['password'])
        if user is not None:
            user.mark_access()
            return user
        raise ex.AuthenticationError("Basic Authentication")


    else:
        token = extract_authorization_token()
        if token is not None:
            try:
                claims = jwt.decode(token, secret_key)
            except ValueError:
                raise ex.AuthenticationError("Invalid Token")

            user_id = None
            ip = None
            if 'user' in claims:
                if 'user_id' in claims['user']:
                    user_id = claims['user']['user_id']
            if 'ip' in claims:
                ip = claims['ip']
            if user_id is None:
                raise ex.AuthenticationError("Invalid Token")
            if str(ip) == str(request.remote_addr):
                user_obj = User.query.filter(User.user_id == user_id).filter(User.active == True).first()
                if user_obj is not None:
                    user_obj.mark_access()
                    return  user_obj
            raise ex.AuthenticationError("Token Authentication")

    return None

def check_authentication(roles=None):
    """
    Authenticate an HTTP request, returning a user if authenticated or None if not.
    """

    if roles is None:
        roles = ['user', 'moderator', 'administrator', 'developer']

    try:
        user = authenticated_user()
        if user is None or user.role not in roles:
            return False

    except ex.AuthenticationError as e:
        return e

    return True


def check_ipthrottle(LIMIT, TIMELIMIT, NAME):

    counter = "aubieapi:ipthrottle:"+NAME
    num = aubieapi.r.llen(counter)

    if num < LIMIT:
        aubieapi.r.lpush(counter, time.time())
        aubieapi.r.expire(counter, TIMELIMIT*2)
        return True
    else:
        t = float(aubieapi.r.lindex(counter, -1))
        if time.time() - t <= TIMELIMIT:
            return False
        else:  # pragma: no cover
            aubieapi.r.lpush(counter, time.time())
            aubieapi.r.rpop(counter)
            aubieapi.r.expire(counter, TIMELIMIT*2)
            return True


def ipthrottle(LIMIT=100, TIMELIMIT=60):
    def wrap(fn):
        def wrapped_f(*args, **kwargs):

            # No remote address in testing (unless we set it to test this)
            if request.remote_addr is None:
                return fn(*args, **kwargs)

            if check_ipthrottle(LIMIT, TIMELIMIT, request.remote_addr):
                return fn(*args, **kwargs)
            else:
                return api_error("Slow down buddy", status=429)

        return wrapped_f
    return wrap


def authenticate(roles=None):
    def wrap(fn):
        def wrapped_f(*args, **kwargs):

            if request.remote_addr is not None:  # Skip this during testing
                hacking = Log.find(code='AUTHFAIL', ip=request.remote_addr, start_date=datetime.datetime.utcnow()-datetime.timedelta(hours=1), count_only=True)
                if  hacking >= 6:
                    return api_authentication_error("Too many failed login attempts. IP banned for 30 minutes.")

            auth_check = check_authentication(roles)
            if auth_check is True:
                return fn(*args, **kwargs)

            return api_authentication_error(auth_check)
        return wrapped_f
    return wrap


def authenticate_local(fn):
    def decorator(*args, **kwargs):

        auth_check = (request.remote_addr == '127.0.0.1' or
                      request.remote_addr is None)

        if auth_check is True:
            return fn(*args, **kwargs)
        Log.logevent('', 'LOCALAUTHFAIL', 'W')
        return api_authentication_error("This API can only be called locally (called from: %s)" % str(request.remote_addr))
    return decorator
